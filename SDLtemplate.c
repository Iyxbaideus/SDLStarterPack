#include<SDL2/SDL.h>
#include<stdlib.h>
#include<sys/time.h>

#define SCALE 1
#define COLS 640
#define ROWS 320
#define FPS 60

int input(SDL_Event Event,int *running){
    switch (Event.type){
/*        case SDL_KEYDOWN:
        break;
        case SDL_KEYUP:
        break;
*/
        case SDL_QUIT:
        *running = 0;
        break;

        default:
        break;
    }
}

void render(SDL_Renderer* renderer,char display[COLS][ROWS]){

    SDL_Rect scaledPixel;  
    scaledPixel.w = SCALE;
    scaledPixel.h = SCALE;

    SDL_SetRenderDrawColor(renderer,0,0,0,255);
    SDL_RenderClear(renderer);

    SDL_SetRenderDrawColor(renderer,255,255,255,255);
    for(int i=0;i<COLS;i++){
        for(int j=0;j<ROWS;j++){
            if(display[i][j]){
                scaledPixel.x=SCALE*i;
                scaledPixel.y=SCALE*j;
                SDL_RenderFillRect(renderer, &scaledPixel);
            }
        }
    }
    SDL_RenderPresent(renderer);
}

int main(){
    
    SDL_Window* window = SDL_CreateWindow("Sapinski",SDL_WINDOWPOS_UNDEFINED,SDL_WINDOWPOS_UNDEFINED,COLS*SCALE,ROWS*SCALE,SDL_WINDOW_RESIZABLE);
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1,SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC); // Création du renderer
    if(renderer == NULL){
        fprintf(stderr,"Erreur lors de la creation d'un renderer : %s",SDL_GetError());
        return EXIT_FAILURE;
    }

    SDL_Event Event;
    int gotEvent;

    struct timeval t1, t2;
    gettimeofday(&t1, NULL);
    srand(t1.tv_usec);
    double timesec=0;
    unsigned long time0=(unsigned long) ( timesec * FPS )%4096+1; /* temporary value for testing frame change */
    unsigned long time= (unsigned long) ( timesec * FPS )%4096;  /* frame number for computing events */

    char display[COLS][ROWS]={0};

    int running=1;

    while(running){

        gotEvent = SDL_PollEvent(&Event);
        while (gotEvent){
            input(Event,&running);
            gotEvent = SDL_PollEvent(&Event);
        }

        if (time0 > time||time==4095) {
            //fprintf(stderr,"frame %ld\n",time0);
            time = time0; /* new frame is activated */
            render(renderer,display);

        }    

        gettimeofday(&t2, NULL);
        timesec = (t2.tv_sec - t1.tv_sec) + ((t2.tv_usec - t1.tv_usec) * 1e-6);
        time0 = (unsigned long) ( timesec * FPS )%4096; /* frame number */

    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
